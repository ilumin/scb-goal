
// (function(window, $) {

var BODY = $('body')
  , NAV = $('.nav')
  , QUESTION_FORM = $('.question-form')
  , QUESTION_CONTROL = $('.question-step-control')
  , QUESTION = $('.question-box')
  , QUESTION_BOUNDARY = { "min": 1, "max": 6 }
  , QUESTION_CURRENT = 1
  , ANSWER = $('.answer-list .choice')
  , ANSWER_INPUT = $('.answer-list input')
  , BUTTON_NEXT = $('.question-step-control .button-next')
  , IMAGES
  ;

IMAGES = [
  '/images/sprite/answer.png',
  '/images/sprite/answer@2x.png',
  '/images/sprite/global.png',
  '/images/sprite/global@2x.png',
  '/images/sprite/home.png',
  '/images/sprite/home@2x.png',
  '/images/sprite/question.png',
  '/images/sprite/question@2x.png',
  '/images/sprite/question-01.png',
  '/images/sprite/question-01@2x.png',
  '/images/sprite/question-02.png',
  '/images/sprite/question-02@2x.png',
  '/images/sprite/question-03.png',
  '/images/sprite/question-03@2x.png',
  '/images/sprite/question-04.png',
  '/images/sprite/question-04@2x.png',
  '/images/sprite/question-05.png',
  '/images/sprite/question-05@2x.png',
  '/images/sprite/question-06.png',
  '/images/sprite/question-06@2x.png',
  '/images/sprite/result.png',
  '/images/sprite/result@2x.png',
];

$('body').append('<div class="preload"><div class="loader"><label class="percent">0%</label>loading...</div></div>');

BUTTON_NEXT.show();

// preload
var loader = new PxLoader();
for (var i = 0; i < IMAGES.length; i++) {
  var pxImage = new PxLoaderImage(IMAGES[i]);
  pxImage.imageNumber = i + 1;
  loader.add(pxImage);
};

// display jscrollpane
var _SCROLLABLE = $('.scrollable');
if (_SCROLLABLE.length > 0) {
  _SCROLLABLE.jScrollPane({
    showArrows: true
  });
}

// append age
var _AGE = $('#select-age');
if (_AGE.length > 0) {
  _AGE.append(function() {
    var _min = 1, _max = 70, _option = '';
    for (var i = _min; i < _max; i++) {
      _option += '<option value="' + i + '">' + i + '</option>';
    }

    return _option;
  });
}

loader.addProgressListener(function(e) {
  $('.preload .percent').html( parseInt(100 * (e.completedCount / e. totalCount), 10) + '%');

  if (e.totalCount <= e.completedCount) {
    setTimeout(function() {
      $('.preload').animate({ opacity: 0 }, function() {
        $(this).remove();
      });

      $('.container').animate({ opacity: 1 });
    }, 1200);
  }
});

loader.start();

if (QUESTION.length>0) {
  QUESTION.not('.question-01').find('.question-content').append(
    '<div class="waiting">' +
    '<div class="sprite-question waiting-capet"></div>' +
    '<div class="sprite-question waiting-man"></div>' +
    '<div class="sprite-question waiting-women"></div>' +
    '</div>'
    );
}

QUESTION.on('q-active', function(e, callback){
  var _this = $(this);

  if (_this.find('.choice input:checked').length <= 0 && !_this.hasClass('question-01')) {
    BUTTON_NEXT.hide();
  }
  else {
    BUTTON_NEXT.show();
  }

  // update subject
  _this.find('.question-subject')
    .css({ transform: 'translateY(-100px)', opacity: 0 });

  // update content position
  _this.find('.question-content')
    .css({ opacity: 0, transform: 'translateY(100px)' });

  // show content
  _this.addClass('active').animate({ opacity: 1 }, function() {
    // show subject
    _this.find('.question-subject').animate({ transform: 'translateY(0)', opacity: 1 });

    // show content
    _this.find('.question-content').animate({ opacity: 1, transform: 'translateY(0)' });

    // update step breadcrumb
    QUESTION_CONTROL.find('.step')
      .attr('class', 'step')
      .addClass( QUESTION.filter('.active').attr('id') );
  });

  if (callback) {
    callback();
  }
});

QUESTION.on('q-deactive', function(e, callback){
  var _this = $(this);

  // hide subject
  _this.find('.question-subject')
    .animate({ transform: 'translateY(-100px)', opacity: 0 });

  // hide content
  _this.find('.question-content').animate({ opacity: 0, transform: 'translateY(100px)' }, function() {
    _this.removeClass('active');
  });

  if (callback) {
    callback();
  }
});

ANSWER.on('a-active', function(e, callback) {
  var _this = $(this);

  _this.addClass('active');

  BUTTON_NEXT.show();

  if (callback) {
    callback();
  }
});

ANSWER.on('a-deactive', function(e, callback) {
  var _this = $(this);

  _this.removeClass('active');

  if (callback) {
    callback();
  }
});

ANSWER_INPUT.change(function() {
  var _this = $(this),
    _selected_value = _this.val(),
    _question = _this.parents('.question-box'),
    _answer = _question.find('.answer-data'),
    _new_answer = _answer.filter('[data-choice=' + _selected_value + ']'),
    _current_answer = _answer.filter('.active')
    ;

  // remove waiting
  _this.parents('.question-box').find('.waiting').hide();

  _new_answer
    .addClass('active')
    .css({ opacity: 0, marginLeft: '20%' })
    .animate({ opacity: 1, marginLeft: '0%' });

  _current_answer
    .animate({ opacity: 0, marginLeft: '-20%' }, 'easeOutBack', function() {
      $(this)
        .removeClass('active')
        .attr('style', '');
    });

});

BODY
  .delegate('.nav-toggle', 'click', function(e) {
    e.preventDefault();

    NAV.toggleClass('active');
  })
  ;

// Question control
QUESTION_CONTROL
  .find('.button').click(function(e) {
    e.preventDefault();

    is_next = $(this).hasClass('button-next');

    if (is_next) {
      // validate
      var validate = false;
      if (QUESTION_CURRENT==1) {
        validate = $('input[name=sex]:checked').val()!=undefined && $('input[name=child]:checked').val()!=undefined && $('select[name=age]').val()!=undefined;
      }
      else {
        validate = $('input[name=question' + QUESTION_CURRENT + ']:checked').val()!=undefined;
      }

      if (!validate) {
        alert( QUESTION_CURRENT==1 ? "กรุณาระบุข้อมูลของท่าน" : "กรุณาระบุข้อมูลของท่าน" );
        return false;
      }
      QUESTION_CURRENT++;
    }
    else {
      QUESTION_CURRENT--;
    }

    if (QUESTION_CURRENT>1) {
      $('.answer-label').show();
    }
    else {
      $('.answer-label').hide();
    }

    if (QUESTION_CURRENT >= QUESTION_BOUNDARY.max) {
      if (QUESTION_CURRENT > QUESTION_BOUNDARY.max) {
        QUESTION_FORM.submit();
        return;
      }

      QUESTION_CURRENT = QUESTION_BOUNDARY.max;
    }

    if (QUESTION_CURRENT <= QUESTION_BOUNDARY.min) {
      QUESTION_CURRENT = QUESTION_BOUNDARY.min;

      QUESTION_CONTROL
        .find('.button-prev').hide();
    }
    else {
      QUESTION_CONTROL
        .find('.button-prev').show();
    }

    QUESTION.filter('.active').trigger('q-deactive', function(){
      QUESTION.eq(QUESTION_CURRENT - 1).trigger('q-active');
    });
  })
  ;

// Answer action
QUESTION.find('.choice').click(function() {
  var _this = $(this),
    _answer_parent = _this.parents('.answer-list'),
    _question_answer = _this.parents('.question-box').find('.answer-data');

  if (_answer_parent.find('.choice.active').length<=0) {
    _this.trigger('a-active');
  }
  else {
    _answer_parent.find('.choice.active').trigger('a-deactive', function() {
      _this.trigger('a-active');
    });
  }
});

// Question 01
var selected = QUESTION.filter('#question-01').find('.selected');
QUESTION.filter('#question-01').find('input').change(function() {
  var _name = $(this).attr('name');
  var _input_sex = $('input[name=sex]');
  var _input_child = $('input[name=child]');
  var _current_sex = _input_sex.filter(':checked').val()=='m' ? 'man' : 'woman';
  var _child = $('.sprite-answer.child');

  $('input[name=' + _name + ']')
    .parent().removeClass('active')
    .end()
    .filter(':checked').parent().addClass('active');

  if (_name == 'child') {
    if (_input_child.filter(':checked').val()=='0') {
      _child
        .removeClass('selected woman man');
    }
    else {
      _child
        .removeClass('selected woman man')
        .delay(600)
        .addClass('selected ' + _current_sex);
    }
  }

  if (_name == 'sex') {
    var _sex = QUESTION.filter('#question-01').find('input:checked').val();

    // flow
    // 1. animate: opacity -> 0, scale -> 0
    // 2. change class
    // 3. animate: opacity -> 1, scale -> 1

    if (_sex=='m') {
      selected
        .animate({ opacity: 0, transform: 'scale(0)' }, function() {
          QUESTION_FORM
            .removeClass('selected-woman')
            .addClass('selected-man');

          $(this).removeClass('woman').addClass('man');
        })
        .animate({ opacity: 1, transform: 'scale(1)' })
        ;

      if (_child.hasClass('selected'))
        _child
          .removeClass('selected woman man')
          .delay(600)
          .addClass('selected man');

      $('.answer-man')
        .filter('.man')
          .animate({ opacity: 0 })
        .end()
        .filter('.man-active')
          .animate({ opacity: 1 });

      $('.answer-woman')
        .filter('.woman')
          .animate({ opacity: 1 })
        .end()
        .filter('.woman-active')
          .animate({ opacity: 0 });
    }
    else if (_sex=='f') {
      selected
        .animate({ opacity: 0, transform: 'scale(0)' }, function() {
          QUESTION_FORM
            .removeClass('selected-man')
            .addClass('selected-woman');

          $(this).removeClass('man').addClass('woman');
        })
        .animate({ opacity: 1, transform: 'scale(1)' })
        ;

      if (_child.hasClass('selected'))
        _child
          .removeClass('selected woman man')
          .delay(600)
          .addClass('selected woman');

      $('.answer-man')
        .filter('.man')
          .animate({ opacity: 1 })
        .end()
        .filter('.man-active')
          .animate({ opacity: 0 });

      $('.answer-woman')
        .filter('.woman')
          .animate({ opacity: 0 })
        .end()
        .filter('.woman-active')
          .animate({ opacity: 1 });
    }
    else {
      // do nothing
    }
  }
});

// register lightbox
window.openRegister = function(action, plan, callback) {
  var content_html = '';
  content_html = '<div class="register-content sprite-result lightbox-bg">'
    + '<h2 class="sprite-result register-subject">กริกข้อมูล</h2>'
    + '<form action="' + action + '" method="post">'
    + '<div class="form-row half"><label>ชื่อ:</label><div class="form-input"><input type="text" name="firstname"></div></div>'
    + '<div class="form-row half"><label>นามสกุล:</label><div class="form-input"><input type="text" name="lastname"></div></div>'
    + '<div class="form-row"><label>อีเมล์:</label><div class="form-input"><input type="text" name="email"></div></div>'
    + '<div class="form-row"><label>เบอร์โทร:</label><div class="form-input"><input type="text" name="telephone"></div></div>'
    + '<input type="hidden" name="plan" value="' + plan + '">'
    + '<input type="submit" class="sprite-result button-submit">'
    + '</form>'
    + '<a href="#" class="sprite-result button-close" onclick="vex.close();">Close</a>'
    + '</div>'
    ;

  //vex.close();

  vex.open({
    content: content_html,
    afterOpen: function($vexContent) {
      if (callback) {
        callback($vexContent);
      }

      return $vexContent;
    }
  });
}

window.openSuccess = function() {
  var content_html = '';
  content_html = '<div class="success-content sprite-result lightbox-bg">'
    + '<h2 class="sprite-result success-subject">Thankyou</h2>'
    + '<div class="success-body">'
    + '<h3>ข้อมูลของคุณได้ถูกส่งไปเรียบร้อยแล้ว</h3><p><strong>คุณ</strong>สามารถร่วมสนุกกับการทายเวลา<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;คุ้มครองเสือร้องไห้จากเหตุไม่คาดฝัน</p>'
    + '<a href="http://1minunexpected.scblife.co.th/" class="sprite-result button-1minute">SCBLife One Minute</a>'
    + '</div>'
    + '<a href="#" class="sprite-result button-close" onclick="vex.close();">Close</a>'
    + '</div>'
    ;

  // vex.close();

  vex.open({
    content: content_html
  });
}

$('body').append('<div class="orientation disable-portrait"><strong>กรุณาวางโทรศัพท์มือถือในแนวนอน</strong></div>');

// })(window, jQuery);
